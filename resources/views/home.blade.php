@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form action="{{ route('save.file') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Occurences</label>
                            {{-- <input type="file" class="form-control-file" name="occurence" id="occurence" accept=".csv" required> --}}
                            <input type="text" class="form-control-file" name="occurence" id="occurence" required>

                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlFile1">Impression</label>
                            {{-- <input type="file" class="form-control-file" name="impression" id="impression" accept=".csv" required> --}}
                            <input type="text" class="form-control-file" name="impression" id="impression" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
