@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <p id="status">Loading.....
                    It may take 30 mins or so.</p>
                    
                    <a href="{{ route('download') }}"> <button type="button" class="btn btn-success" style="display:none;" id="btn_download">Download</button> </a>
                    
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
@section('page_scripts')

<script>
window.onload = function(){
    console.log('hi');
            var _token = $('input[name="_token"]').val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                url:"{{ route('merge') }}",
                method:"GET",
                data:{
                _token:_token},
                success:function(data){
                    console.log(data);
                    document.getElementById("btn_download").style.display = "block";
                    document.getElementById("status").innerHTML = "Done! You may download your file now.";
                }, error: function(a,b,c) {
                    console.log(a);
                }
            });
}

    


</script>

@endsection