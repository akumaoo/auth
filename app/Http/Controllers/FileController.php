<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Response;

class FileController extends Controller
{
    
    public function saveFile(Request $request){
        // Storage::disk('public_uploads')->delete('this_Occurences');
        // Storage::disk('public_uploads')->delete('this_Impressions');
        $file_type='.csv';
        $occurence_path='Occurrences/2019/01 - January 2019/'.$request->occurence.$file_type;
        $impression_path='Impressions/2019/01 - January/'.$request->impression.$file_type;

        // return $occurence_path.' '. $impression_path;

        $occ_exist=Storage::disk('sftp')->exists($occurence_path);
        $imp_exist=Storage::disk('sftp')->exists($impression_path);

        if($occ_exist && $imp_exist){
            $occurence_sftp=Storage::disk('sftp')->get($occurence_path);
            $impression_sftp=Storage::disk('sftp')->get($impression_path);

            Storage::disk('public_uploads')->put('this_Occurences'.$file_type,$occurence_sftp);
            Storage::disk('public_uploads')->put('this_Impressions'.$file_type,$impression_sftp);

            return view('loading');
        }
        else{
            return 'File Not Found';
        }
    }

    public function mergeFiles(){
        return exec('uploads/files/a.out');
    }

    public function downloadFile(){
        // $headers = [
        //     'Content-Type' => 'application/csv',
        //  ];
        return Storage::disk('public_downloads')->download('/merged.csv');
    }
        
}
