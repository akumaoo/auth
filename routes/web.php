<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/save', 'FileController@saveFile')->name('save.file');
Route::get('/loading', 'FileController@mergeFiles')->name('merge');
Route::get('/download', 'FileController@downloadFile')->name('download');
